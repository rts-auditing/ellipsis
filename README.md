# Ellipsis

Ellipsis is a kernel-based log reduction framework that leverages the predictability of real-time taskset's execution profile to dramatically reduce event count and storage overheads of system auditing in real-time systems.

# Content Overwiew
The following table gives an overview of the files included.
Utility and library files are not described in the table.
Raw logs are not included due to size and plottting scripts that use the raw data to generate plots are also not included.


| File/Dir                                                                              | Description       | Paper Sections    |
| -------------                                                                         | -------------     | -------------     |
| [src/template-gen.py](src/template-gen.py)                                            | Template Generation | Section 3 |
| [patchset/ellipsis-kernel.patch](patchset/ellipsis-kernel.patch)                      | Kernel Patch      | Section 3         |
| [patchset/ellipsis-audit-userspace.patch](patchset/ellipsis-audit-userspace.patch)    | Auditctl Patch    | Section 3         |
| [src/template-gen-tc.py](src/template-gen-tc.py)                                      | Template temporal Contraints | Section 3 |
| [src/microbenchmark](src/microbenchmark) <br> [src/microbenchmark_runner.py](src/microbenchmark_runner.py) | Microbenchmarks for Audit Framework Analysis | [Appendix A](Appendices\A_LinuxAuditAnalyses.pdf) |




# Evaluation Platform
We have evaluated our implementation on the following setup:

* Linux kernel 4.19 RT [(Source)](https://github.com/raspberrypi/linux/tree/rpi-4.19.y-rt)
* Linux Audit enabled in kernel options
* Raspberry Pi 4B (4GB)
* Audit userspace utilities v2.8.6 [(Source)](https://github.com/linux-audit/audit-userspace)

# Project Setup

## Kernel Patches
To create a patched kernel, we recommend the following steps:

* Fetch the Linux Kernel source from https://github.com/raspberrypi/linux/tree/rpi-4.19.y-rt. Specifically, at commit id [1c082fa](https://github.com/raspberrypi/linux/commit/1c082fa78a09436ff22f30bfc82b9b7a127686d5).
* Apply the [ellipsis kernel patch](patchset/ellipsis-kernel.patch) on the source
* Build the 32 bit kernel image for RPi-4 by following the steps here - https://www.raspberrypi.org/documentation/linux/kernel/building.md but enable CONFIG_AUDIT.

* Also install headers to be able to use modified audit userspace
```bash
sudo env PATH=$PATH make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- headers_install INSTALL_HDR_PATH=mnt/ext4/usr/
```


## Audit Userspace Patches
To enable/disable ellipsis features and update ellipisis templates, we need to apply a patch to the audit userspace tools using the below steps:

* Fetch the Audit Userspace v2.8.6 source from https://github.com/linux-audit/audit-userspace/tree/2.8_maintenance, at commit id [80866dc](https://github.com/linux-audit/audit-userspace/commit/80866dc78b5db17010516e24344eaed8dcc6fb99)
* Apply the [ellipsis userspace patch](patchset/ellipsis-audit-userspace.patch) on the source
* Build the updated audit-userspace project using the steps provided in the [installation guide](https://github.com/linux-audit/audit-userspace/blob/2.8_maintenance/INSTALL.tmp). The steps we used are added to the README in audit-userspace with the provided patch.
* some packages may be required
```bash
sudo apt install swig libwrap0-dev libcap-ng-utils libtool shtool autoconf
```

We recommend not overwriting the default audit-userspace installation, instead build the project locally and provide correct references in the file [src/ellipsis/ellipsisctl.py](src/ellipsis/ellipsisctl.py).

# Usage Instructions

## Configuration
Before generating Ellipsis templates for a new application, we need to update existing configuration options in the [configuration file](src/ellipsis.cfg)

A few of the options one would commonly need to update are:

* `syscallsToAudit` - List of system calls that need to be audited
* `bufferSize` - Size of the audit buffer. May need to be updated depending on the underlying hardware and the audit rules to ensure audit logs are not lost during the template generation phase.
* `templateRootFolder` - Location to store templates
* `timeout` - Run application for `timeout` seconds to capture audit logs for template generation
* `tc_timeout` - Run applcations for `tc_timeout` secondsd to capture audit logs for the temporal constraints phase.
* `cmdline` - Command for launching the application along with agruments

Timeout configuration options are provided separately for the 2 stages as we might need longer runs and are not as constrained by audit losses when inferring temporal constraints.

## Generating Templates
The first phase of generating Ellipsis templates can be launched by running `python3 template-gen.py <APPLICATION>` in the `src/` directory.

After a successful run, template files for each thread/task can be found at  `<templateRootFolder>/<APPLICATION>/<thread-name>`.

The second phase for inferring temporal constraints is optional, and can be launched by running `python3 template-gen-tc.py <APPLICATION>` in the `src/` directory.

For applications where the expected task durations and inter-arrival times for tasks are known beforehand, one can modify the templates directly without running the second phase.

## Auditing with Ellipsis
Ellipsis templates are loaded using `<path-to-patched-audit-userspace>/src/auditctl  -z file=<path-to-template-file>`.

Ellipsis can be toggled by `<path-to-patched-audit-userspace>/src/auditctl -T [0,1]`.

Ellipsis-HP can be further toggled using `<path-to-patched-audit-userspace>/src/auditctl -M [0,1]`. Ellipsis needs to be enabled before Ellipsis-HP can be toggled.

Templates can be cleared from kernel memory using `<path-to-patched-audit-userspace>/src/auditctl  -X`

## Applications
The evaluated applications can be fetched from their respective repositories.
* [ArduPilot](https://ardupilot.org/)
* [Motion](https://motion-project.github.io/index.html)
