from auditlib import auditctl
from ellipsis import ellipsisctl,template
import subprocess, time, json, os, sys, atexit, random

OUT_FILE = "scaling/res_isol.json"
TASK_RUNNER = "/home/pi/rt-audit-internal/src/bin/scaling"
syscall_ids = [20] # getpid()
SAMPLES = 100

def cleanup():
    cleanup_all(True)
    with open(OUT_FILE, 'w') as resultfile:
        json.dump(res, resultfile, indent=4)

def setup_perf_mode():
    with(open('/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor','w')) as f:
            f.write("performance")
    time.sleep(1)

def cleanup_all(ellipsis_enabled = True):
    # ellipsisctl.toggle_ellipsis2(0, True)
    ellipsisctl.toggle_ellipsis(0, True)
    if ellipsis_enabled: ellipsisctl.clear_templates(True)
    auditctl.disable_audit(True)
    # auditctl.wait_till_backlog_clear()
    # auditctl.clear_audit_logs()
    # auditctl.clear_stats(True)


def run_one(case, reps):
    auditctl.wait_till_backlog_clear()
    auditctl.clear_audit_logs()
    # auditctl.clear_stats(True)

    if case == "Audit":
        result = subprocess.run([TASK_RUNNER, str(reps), str(random.randint(0, 1))],
                                 universal_newlines=True, capture_output=True)
    elif case == "Ellipsis_Success":
        result = subprocess.run([TASK_RUNNER, str(reps), str(1)],
                                 universal_newlines=True, capture_output=True)
    elif case == "Ellipsis_Fail":
        result = subprocess.run([TASK_RUNNER, str(reps), str(0)],
                                 universal_newlines=True, capture_output=True)

    auditctl.wait_till_backlog_clear()
    logs_lost = auditctl.get_lost_logs()
    assert(logs_lost == 0)
    # auditctl.clear_audit_logs()
    # auditctl.clear_stats(True)

    # print("ret:", result.returncode)
    # print("err:", result.stderr)
    out = result.stdout
    return int(out)


atexit.register(cleanup)
setup_perf_mode()
cleanup_all(True)
auditctl.clear_syscall_rules()
auditctl.add_syscall_rule('getpid,getppid', TASK_RUNNER)
auditctl.set_buffer_size(50000, True)

# Warmup
run_one("Audit", 10)
run_one("Ellipsis_Fail", 10)

res = []
for reps in range(10, 301, 10):
    print("Len:", reps)
    res.append([])
    res[-1].append(reps)
    cleanup_all(True)

    # First run Audit
    res[-1].append([])
    auditctl.enable_audit(True)
    for i in range(SAMPLES):
        res[-1][-1].append(run_one("Audit", reps))

    # Set up Ellipsis
    syscall_seq = [syscall_ids[0]] * reps
    filename = template.create_template_file(str(len(syscall_seq)), syscall_seq)
    ellipsisctl.load_template(filename, True)
    ellipsisctl.toggle_ellipsis(1, True)
    os.remove(filename)

    # Run test with success
    res[-1].append([])
    for i in range(SAMPLES):
        res[-1][-1].append(run_one("Ellipsis_Success", reps))

    # Run test with failure
    res[-1].append([])
    for i in range(SAMPLES):
        res[-1][-1].append(run_one("Ellipsis_Fail", reps))
