import re, os, subprocess, time, sys, argparse, configparser, pathlib
from task import TaskRunner
from decimal import *
from collections import Counter, defaultdict
#import pandas as pd
from auditlib.audit_parser import *
import config
from auditlib import auditctl
from ellipsis import ellipsisctl

def extract_temporal_constraints(filename):
    tc_map = dict()
    with open(filename,"r") as f:
        for line in f.readlines():
            if 'type=SYSCALL' not in line:
                continue
            if 'template=' not in line:
                continue

            template = capture_template_name(line)
            start_time = capture_start_time(line)
            end_time = capture_end_time(line)
            duration = end_time - start_time

            if(template not in tc_map.keys()):
                tc_map[template] = dict()

            tpl_map = tc_map[template]
            duration_list = tpl_map.get('duration',[])
            duration_list.append(duration)
            tc_map[template]['duration'] = duration_list

            act_time_list = tpl_map.get('act_time',[])
            act_time_list.append(start_time)
            tc_map[template]['act_time'] = act_time_list

    for tpl in tc_map.keys():
        tc_map[tpl]['duration'] = max(tc_map[tpl]['duration'])
        diff_list = []
        for x, y in zip(tc_map[tpl]['act_time'][0::], tc_map[tpl]['act_time'][1::]):
            diff_list.append(y-x)
        tc_map[tpl]['inter_arrival_time'] = max(diff_list)
        del tc_map[tpl]['act_time']

    return tc_map

def update_template_with_tc(task_name, template_folder,task_tc_map=dict()):
    tpl_files = pathlib.Path(template_folder).glob('**/*.template')
    for tpl_file in tpl_files:
        print("Loading template : ", tpl_file)
        outputFilename = tpl_file.parent / (tpl_file.name + '.tc')
        with tpl_file.open(mode="r",encoding="utf-8") as f, outputFilename.open(mode="w",encoding="utf-8") as f1:
            task_name = f.readline().strip()
            f1.write(task_name + "\n")
            f1.write(f.readline())
            f1.write(str(task_tc_map[task_name]['duration']) + "\n")
            f1.write(str(task_tc_map[task_name]['inter_arrival_time']) + "\n")
            f1.writelines(f.readlines())

    return

def main(args, cfg):

    task_name = args.taskset
    cmdline = config.get_cmdline(task_name, cfg)
    task_runner = TaskRunner(task_name, cmdline)
    task_params = config.get_task_params(task_name, cfg)
    template_folder = config.get_template_folder(task_name,cfg)
    
    print(task_name, task_params)

    executable = cmdline[0]
    auditctl.set_buffer_size(config.get_buffer_size(task_name,cfg))
    auditctl.clear_syscall_rules()
    auditctl.add_syscall_rule(config.get_syscalls_to_audit(task_name,cfg),executable)

    #load templates for each thread and run the application with ellipsis enabled
    tpl_files = pathlib.Path(template_folder).glob('**/*.template')
    for tpl_file in tpl_files:
        print("Loading template : ", tpl_file)
        ellipsisctl.load_template(str(tpl_file),suppress_output=False)

    auditctl.clear_audit_logs()
    auditctl.clear_stats()
    auditctl.enable_audit()

    ellipsisctl.toggle_ellipsis(1, suppress_output=False)
    #parse the logs, this time looking for the start and end times for each match
    task_runner.run(task_params,tc=True)
    ellipsisctl.toggle_ellipsis(0, suppress_output=False)
    auditctl.disable_audit()
    auditctl.wait_till_backlog_clear()
    log_file="/tmp/audit.tc.log"
    auditctl.save_audit_logs(log_file)
    ellipsisctl.clear_templates()
    task_tcs = extract_temporal_constraints(log_file)
    print(task_tcs)
    update_template_with_tc(task_name,template_folder,task_tcs)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Template Generation Script')
    parser.add_argument('taskset',help='Name of taskset to be used for template generation')
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read('ellipsis.cfg')

    main(args, cfg)
