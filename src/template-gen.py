#!/usr/bin/python3


"""
template-gen.py generates a template representation given an audit log containing only syscall lines
"""

import re, os, subprocess, time, sys, argparse, configparser, pathlib, shutil
from task import TaskRunner
from decimal import *
from collections import Counter, defaultdict
#import pandas as pd
from auditlib.audit_parser import *
import config
from auditlib import auditctl
from ellipsis import ellipsisctl
from pprint import pprint

IGNORED_ARG = '-1'

def _convert_args(hex_args):
    if len(hex_args)%2 == 1:
        hex_args = '0' + hex_args
    return int.from_bytes(bytes.fromhex(hex_args),byteorder='big',signed=True)

def _format_log_line(syscall,a0=IGNORED_ARG,a1=IGNORED_ARG,a2=IGNORED_ARG,a3=IGNORED_ARG):
    return "{}:{}:{}:{}:{}".format(syscall,a0,a1,a2,a3)

def format_log_line(syscall,args):
    if(syscall in (SYSCALL_MAP['open'],SYSCALL_MAP['close'],SYSCALL_MAP['ioctl'])):
        return  _format_log_line(syscall,a0=_convert_args(args[0]))
    elif (syscall in (SYSCALL_MAP['read'],SYSCALL_MAP['pread64'],SYSCALL_MAP['write'])):
        return _format_log_line(syscall,a0=_convert_args(args[0])) #,a2=_convert_args(args[2]))
    elif (syscall == SYSCALL_MAP['execve']):
        return _format_log_line(syscall,args[0],args[1],args[2],args[3])
    elif (syscall == SYSCALL_MAP['openat']):
        return _format_log_line(syscall, a0=_convert_args(args[0]), a1=_convert_args(args[1]),a2=_convert_args(args[2]),a3=_convert_args(args[3]))
    else:
        return _format_log_line(syscall)

def find_splits(sequence, boundary_system_call_nums):
    return [i for i, x in enumerate(sequence) if x[1] in
        (_format_log_line(syscall_no) for syscall_no in boundary_system_call_nums )]

def squash_log_lines(sequence):
    squashed_output = []
    prev_elem = -1
    for elem in sequence:
        if elem != prev_elem+1 and prev_elem >= 0:
            squashed_output.append((prev_elem,elem))
        prev_elem = elem

    return squashed_output

def get_log_line(sequence):
    return sequence[1]

def get_timestamp(sequence):
    return sequence[0]

def get_sequence_identifier(sequence):
    return ','.join(map(get_log_line,sequence))

def most_frequent_substring(indexes,sequence,n):
    freq = Counter()
    for start,end in indexes:
        seq = sequence[start:end]
        seq_id = get_sequence_identifier(seq)
        freq[seq_id] += 1

    most_common_seq_ids = freq.most_common()
    return [(k,count) for k,count in most_common_seq_ids if (count >= 1 and k.count(',') >= 1)]

def print_templates_to_file(task_name, thread_templates_map, template_folder,task_tc_map=dict()):
    for thread,tpls in thread_templates_map.items():
        #create folder
        task_folder_path = pathlib.Path(template_folder).joinpath(thread)
        task_folder_path.mkdir(parents=True,exist_ok=True)
        os.chmod(task_folder_path, 0o777)
        thread_tc_map = task_tc_map.get(thread,{})
        #create per thread folders
        for idx,tpl in enumerate(tpls):
            template_entries = tpl[0].split(',')[1:]

            with (task_folder_path / (str(idx) + '.template')).open(mode="w",encoding="utf-8") as f:
                f.write(thread + "\n")
                f.write(str(len(template_entries)) + "\n")
                f.write(str(thread_tc_map.get('duration',0)) + "\n")
                f.write(str(thread_tc_map.get('inter-task',0)) + "\n")
                for entry in template_entries:
                    f.write("{}:{}\n".format(entry,0))
            os.chmod(task_folder_path / (str(idx) + '.template'), 0o777)

            with (task_folder_path / 'summary').open(mode="a",encoding="utf-8") as f:
                f.write(str(idx) + " : " + str(tpl[1]) + '\n')
    return

def parse_audit_file(filename, boundary_system_call_nums):
    result = {}
    print("Creating template from file : ",filename)
    thread_id_map = {}
    with open(filename,"r") as f:
        for line in f.readlines():
            if 'type=SYSCALL' not in line:
                continue

            syscall = capture_syscall(line)
            thread_id = capture_thread_id(line)
            command = capture_comm(line)
            args = capture_args(line)
            timestamp = capture_time(line)

            thread_audits = thread_id_map.get((command),[])
            log_line = format_log_line(syscall,args)
            thread_audits.append((timestamp,log_line))
            thread_id_map[(command)] = thread_audits

    print(thread_id_map.keys())
    for k in thread_id_map.keys():
        sequence = thread_id_map[k]
        sorted(sequence, key=lambda x:x[0])
        indexes = squash_log_lines(find_splits(sequence,boundary_system_call_nums))
        res = most_frequent_substring(indexes, sequence,1)
        result[k] = res

    return result

def main(args, cfg):

    task_name = args.taskset
    cmdline = config.get_cmdline(task_name, cfg)
    task_runner = TaskRunner(task_name, cmdline)
    task_params = config.get_task_params(task_name, cfg)
    boundary_system_call_nums = auditctl.get_boundary_syscall_numbers(config.get_boundary_syscalls(task_name, cfg).split(','))
    template_folder = config.get_template_folder(task_name,cfg)
    if os.path.exists(template_folder): shutil.rmtree(template_folder)
    print(task_name, task_params)

    executable = cmdline[0]
    auditctl.set_buffer_size(config.get_buffer_size(task_name,cfg))
    auditctl.clear_syscall_rules()
    auditctl.add_syscall_rule(config.get_syscalls_to_audit(task_name,cfg) + "," + config.get_boundary_syscalls(task_name,cfg),executable)

    auditctl.clear_audit_logs()
    auditctl.clear_stats()
    auditctl.enable_audit()

    task_runner.run(task_params)

    auditctl.disable_audit()
    auditctl.wait_till_backlog_clear()
    log_file = "/tmp/audit.log"
    auditctl.save_audit_logs(log_file)
    task_template_map = parse_audit_file(log_file, boundary_system_call_nums)
    pprint(task_template_map)
    print_templates_to_file(task_name,task_template_map,template_folder)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Template Generation Script')
    parser.add_argument('taskset',help='Name of taskset to be used for template generation')
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read('ellipsis.cfg')

    main(args, cfg)
