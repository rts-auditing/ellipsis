import subprocess, os, pathlib

AVAILABLE_TASKSETS = ["ARDUCOPTER","MOTION","PWM_ATTACK"]

#task params that we need at init - cmd line args,
#task params that we need at runtime - capture output = True/False, iters, do we capture timing data
#arducopter - cmd, op, env
class TaskRunner:

    def __init__(self, task_name,cmdline,attack_name="",attack_cmdline=""):
        if not(task_name in AVAILABLE_TASKSETS):
            raise ValueError("Invalid task name")

        self.task_name = task_name
        self.cmdline = cmdline
        self.attack_name = attack_name
        self.attack_cmdline = attack_cmdline

    def __run_arducopter(self, task_params, tc=False):

        my_env = os.environ.copy()
        my_env['ARDU_TIME'] = ('1' if task_params['capture_timing'] else '0' )
        my_env['ARDU_ITER'] = str(task_params['tc_iters']) if tc else str(task_params['iters'])

        process = subprocess.run(self.cmdline,universal_newlines=True,capture_output=task_params['capture_console'],env=my_env)
        #TODO: We can figure out what to do with program output here

    def __run_till_timeout(self, task_params, tc=False):
        timeout = task_params['tc_timeout'] if tc else task_params['timeout']
        try:
            process = subprocess.run(self.cmdline,universal_newlines=True,capture_output=False,timeout=timeout)
        except subprocess.TimeoutExpired:
            print("Process exited as timeout {} expired".format(timeout))

    def run(self, task_params, tc=False):
        if self.task_name == "ARDUCOPTER":
            self.__run_arducopter(task_params, tc)
        else:
            self.__run_till_timeout(task_params, tc)
