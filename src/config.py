
def get_syscalls_to_audit(task, config):
    return str(config[task].get('syscallsToAudit',config['DEFAULT']['syscallsToAudit']))

def get_boundary_syscalls(task,config):
    return str(config[task].get('boundarySystemCalls',config['DEFAULT']['boundarySystemCalls']))

def get_buffer_size(task, config):
    return int(config[task].get('bufferSize',config['DEFAULT']['bufferSize']))

def get_task_executable(task,config):
    return get_cmdline(task, config)[0]

def get_cmdline(task, config):
    return config[task]['cmdline'].split()

def get_task_params(task,config):
    params = {}
    if task == 'ARDUCOPTER':
        params['capture_console'] = bool(config[task]['captureConsole'])
        params['iters'] = int(config[task]['iters'])
        params['capture_timing'] = bool(config[task]['captureTiming'])
        params['tc_iters'] = int(config[task]['tc_iters'])
    else:
        params['timeout'] = int(config[task].get('timeout'))#, config['DEFAULT']['timeout'])
        params['tc_timeout'] = int(config[task].get('tc_timeout'))#, config['DEFAULT']['tc_timeout'])

    return params

def get_template_folder(task, config):
    return str(config['DEFAULT']['templateRootFolder']) + "/" + task