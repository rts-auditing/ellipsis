import re, os, subprocess, time, sys, argparse, configparser, pathlib
from task import TaskRunner
from decimal import *
from auditlib.audit_parser import *
import config
from auditlib import auditctl
from ellipsis import ellipsisctl


def run(task_name, audit_sys, output_file, attack, cfg):

    cmdline = config.get_cmdline(task_name, cfg)
    attack_task = ""
    attack_cmdline = ""
    task_params = config.get_task_params(task_name, cfg)
    template_folder = config.get_template_folder(task_name, cfg)
    executable = cmdline[0]

    if attack:
        attack_task = attack
        attack_cmdline = config.get_cmdline(attack_task, cfg)
        attack_params = config.get_task_params(attack_task, cfg)
        for k,v in attack_params.items():
            task_params[k] = v
        executable = None   #Reset executable as we want to audit the larger context

    task_runner = TaskRunner(task_name, cmdline, attack_task, attack_cmdline)

    print(task_name, attack_task, task_params, executable)

    #Check and initialize correct audit system
    auditctl.set_buffer_size(config.get_buffer_size(task_name,cfg))
    auditctl.clear_syscall_rules()
    auditctl.add_syscall_rule(config.get_syscalls_to_audit(task_name,cfg),executable)

    auditctl.clear_audit_logs()
    auditctl.clear_stats()
    auditctl.enable_audit()

    if audit_sys == 'ELLIPSIS' or audit_sys == 'ELLIPSIS-HP':
        #load templates in folder template
        tpl_files = pathlib.Path(template_folder).glob('**/*.template')
        for tpl_file in tpl_files:
            print("Loading template : ", tpl_file)
            ellipsisctl.load_template(str(tpl_file),suppress_output=False)

        ellipsisctl.toggle_ellipsis(1,suppress_output=False)

    if audit_sys == 'ELLIPSIS-HP':
        ellipsisctl.toggle_ellipsis2(1,suppress_output=False)

    auditctl.clear_audit_logs()
    task_runner.run(task_params)

    if audit_sys == 'ELLIPSIS-HP':
        ellipsisctl.toggle_ellipsis2(0, suppress_output=False)

    if audit_sys == 'ELLIPSIS' or audit_sys == 'ELLIPSIS-HP':
        ellipsisctl.toggle_ellipsis(0, suppress_output=False)
        ellipsisctl.clear_templates()

    auditctl.disable_audit()

    auditctl.wait_till_backlog_clear()

    auditctl.save_audit_logs(output_file)

    return

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Template Generation Script')
    parser.add_argument('taskset',help='Name of taskset to be used')
    parser.add_argument('audit_system',help='Audit System to be used')
    parser.add_argument('output_file', help='Path to output file')
    parser.add_argument('attack',help='Name of attack to run')
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read('ellipsis.cfg')

    run(args.taskset, args.audit_system, args.output_file, args.attack, cfg)
