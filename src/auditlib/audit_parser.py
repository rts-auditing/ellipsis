import re

SYSCALL_MAP = {
    'open' : 5,
    'openat': 322,
    'read' : 3,
    'pread64' : 180,
    'write' : 4,
    'close' : 6,
    'execve' : 11,
    'select' : 82,
    'sched_yield' : 158,
    'nanosleep' : 162,
    'clock_nanosleep' : 265,
    'epoll_wait' : 252,
    'ioctl' : 54
}

def capture_thread_id(line):
    THREAD_ID_REGEX = re.compile(r"tid=(\d+)")
    match = THREAD_ID_REGEX.search(line)
    return match.group(1)

def capture_syscall(line):
    SYSCALL_REGEX = re.compile(r"syscall=(\d+)")
    match = SYSCALL_REGEX.search(line)
    return int(match.group(1))

def capture_args(line):
    ARGS_REGEX = re.compile(r"a[0-3]=([a-zA-Z0-9]+)")
    return ARGS_REGEX.findall(line)

def capture_seq_id(line):
    SEQ_ID = re.compile(r":(\d+)")
    return int(SEQ_ID.search(line).group(1))

def capture_comm(line):
    COMMAND_REGEX = re.compile(r"comm=\"([a-zA-Z0-9\-]+)\"")
    match = COMMAND_REGEX.search(line)
    if match is None:
        print(line)
    return match.group(1)

def capture_time(line):
    TIME_REGEX = re.compile(r"msg=audit\((\d+)\.(\d+):\d+\)")
    match = TIME_REGEX.search(line)
    return int(match.group(1)) * 10 **9 + int(match.group(2))   #return timestamp in nanos

def capture_start_time(line):
    START_REGEX = re.compile(r"stime=(\d+)")
    match = START_REGEX.search(line)
    return int(match.group(1))

def capture_end_time(line):
    END_REGEX = re.compile(r"etime=(\d+)")
    match = END_REGEX.search(line)
    return int(match.group(1))

def capture_template_name(line):
    TEMPLATE_REGEX = re.compile(r"template=([a-zA-Z0-9\-]+)")
    match = TEMPLATE_REGEX.search(line)
    return match.group(1)
