import subprocess, shutil, glob, os, time

ALL_SYSCALLS = "execve,read,readv,write,writev,sendto,recvfrom,sendmsg,recvmsg,mmap,,mmap2,llseek,fstat64,openat,mprotect,link,symlink,clone,fork,vfork,open,close,creat,openat,mknodat,mknod,dup,dup2,dup3,bind,accept,accept4,connect,rename,setuid,setreuid,setresuid,chmod,fchmod,pipe,pipe2,truncate,ftruncate,sendfile,unlink,unlinkat,socketpair,splice,init_module,finit_module"
CUSTOM_AUDITCTL_PATH = "/home/pi/audit-userspace/src/auditctl"

def reset_audit():
    clear_audit_logs()
    clear_syscall_rules()
    clear_stats()

def clear_audit_logs():
    with open('/var/log/audit/audit.log','w') as fd:
        pass
    for filePath in glob.glob('/var/log/audit/audit.log.*'):
        os.remove(filePath)

def enable_audit(suppress_output=False):
    subprocess.run(['auditctl','-e','1'],capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def disable_audit(suppress_output=False):
    subprocess.run(['auditctl','-e','0'],capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def set_buffer_size(buf_size,suppress_output=False):
    subprocess.run(['auditctl','-b',str(buf_size)],capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def add_syscall_rule(syscalls,executable=None):
    if executable is None:
        p = subprocess.run(['auditctl','-a','always,exit','-F','arch=b32','-S',str(syscalls)])
    else:
        executableFilter = 'exe=' + executable
        p = subprocess.run(['auditctl','-a','always,exit','-F','arch=b32','-S',str(syscalls),'-F',str(executableFilter)])
    subprocess.run(['auditctl','-l'])

def clear_syscall_rules():
    subprocess.run(['auditctl','-D'])

def save_audit_logs(outputfile):
    shutil.copyfile('/var/log/audit/audit.log',outputfile)
    return

def clear_stats(suppress_output=False):
    subprocess.run(['auditctl','--reset-lost'],capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def get_lost_logs():
    process = subprocess.run(['auditctl','-s'],universal_newlines=True,capture_output=True)
    for line in process.stdout.split("\n"):
        if line.startswith("lost"):
            return int(line.split()[1])

def get_backlog():
    process = subprocess.run(['auditctl','-s'],universal_newlines=True,capture_output=True)
    for line in process.stdout.split("\n"):
        if line.split()[0] == "backlog":
            return int(line.split()[1])
    return 0

def wait_till_backlog_clear(suppress_output=True):
    loop = 0
    while get_backlog() > 0:
        loop += 1
        if not suppress_output:
            print ("backlog clear wait is looping", loop)
        time.sleep(0.1)

def get_status():
    subprocess.run(['auditctl','-s'],universal_newlines=True)
    print("\n")

def set_backlog_wait_time(t = 0, suppress_output = False):
    subprocess.run(['auditctl','--backlog_wait_time', str(t)], capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def get_syscall_number(syscall_name):
    process = subprocess.run(['ausyscall',syscall_name],universal_newlines=True, capture_output=True)
    return int(process.stdout.split("\n")[0].split()[-1])

def get_boundary_syscall_numbers(syscall_names):
    return [get_syscall_number(syscall_name) for syscall_name in syscall_names]
