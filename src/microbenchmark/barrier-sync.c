#define _GNU_SOURCE
#include <sched.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/random.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

//Include microbenchmark handlers
#include "syscalls/process.h"
#include "syscalls/fs.h"
#include "syscalls/net.h"
#include "syscalls/mem.h"

#define REPS 100
#define SIZE 4096

int REPETITIONS = REPS;
int SYSCALLOPTION = -1;
//int numThreads = 0;

pthread_barrier_t barrier;
char* syscallName;

long long int LAT[4][5005];

long long timespec_subtract(struct timespec *a, struct timespec *b)
{
	long long ns;
	ns = (b->tv_sec - a->tv_sec) * 1000000000LL;
	ns += (b->tv_nsec - a->tv_nsec);
	return ns;
}

void run_mmap2(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
	    int* addr = (int *)mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);

        *addr = 1;
        munmap(addr, SIZE);
}

//void (*syscall_method_arr[])(struct timespec*,struct timespec*,struct timespec*,struct timespec*) = {run_getpid,run_mmap2,run_write,run_read,run_close,run_openat,run_connect,run_mprotect,run_fstat64,run_llseek,run_pread64};
void (*syscall_method_arr[])(struct timespec*,struct timespec*,struct timespec*,struct timespec*) = {run_getpid,run_getppid,run_getgid,run_getuid,run_getpgrp,run_geteuid};

void *run_benchmark(int core){
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);

	int coreNumber = sched_getcpu();
	fprintf(stderr,"Core Number :%d\n",coreNumber);
	int i;

	//Setup sched affinity to the core
	for (i = 0; i < REPETITIONS; i++) {
		struct timespec start_data, stop_data, empty_start, empty_stop;

		(syscall_method_arr[SYSCALLOPTION])(&start_data, &stop_data, &empty_start, &empty_stop);

		long long duration = timespec_subtract(&start_data,&stop_data)-timespec_subtract(&empty_start,&empty_stop);
		if(duration > 0){
		LAT[coreNumber][i] = duration;
		} else{
			printf("%d : %lld\n",i,duration);
		}
    }

}

int main(int argc, char* argv[])
{
    int *addr;
    int i; 
	
    int numThreads = 0;

    if(argc == 4){
    	REPETITIONS = atoi(argv[1]);
		numThreads = atoi(argv[2]);
		fprintf(stderr,"REPS overridden to %d\n",REPETITIONS);
		SYSCALLOPTION = atoi(argv[3]);
    }


    i = numThreads;

	run_benchmark(3);

    int j;
    for(j=0; j < REPETITIONS; j++){
	    for(int i = 0 ; i< numThreads; i++){
		    printf("Net latency for core %d iteration %d is %lld ns\n",3-i,j,LAT[3-i][j]);
	    }
    }

    return 0;
}

