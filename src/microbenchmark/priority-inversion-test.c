#define _GNU_SOURCE
#include <sched.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/random.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <pthread.h>

//Include microbenchmark handlers
#include "syscalls/process.h"
#include "syscalls/fs.h"
#include "syscalls/net.h"
#include "syscalls/mem.h"

#define REPS 100
#define SIZE 4096

int REPETITIONS = REPS;
int SYSCALLOPTION = -1;
int RT_PRIORITY;

pthread_barrier_t barrier;
char* syscallName;

long long int LAT[4][5005];

struct sched_param param;


long long timespec_subtract(struct timespec *a, struct timespec *b)
{
	long long ns;
	ns = (b->tv_sec - a->tv_sec) * 1000000000LL;
	ns += (b->tv_nsec - a->tv_nsec);
	return ns;
}

void run_mmap2(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
		clock_gettime(CLOCK_MONOTONIC,start_data);
	    int* addr = (int *)mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
		clock_gettime(CLOCK_MONOTONIC,stop_data);

		clock_gettime(CLOCK_MONOTONIC,empty_start);
		clock_gettime(CLOCK_MONOTONIC,empty_stop);

        *addr = 1;
        munmap(addr, SIZE);
}

void (*syscall_method_arr[])(struct timespec*,struct timespec*,struct timespec*,struct timespec*) = {run_getpid,run_mmap2,run_write,run_read,run_close,run_openat,run_connect,run_mprotect,run_fstat64,run_llseek};

void *run_benchmark(int core){
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);
	if(RT_PRIORITY){
		pthread_setschedparam(pthread_self(),SCHED_FIFO,&param);
	}

	int coreNumber = sched_getcpu();
	fprintf(stderr,"Core Number :%d\n",coreNumber);
	pthread_barrier_wait(&barrier);
	int i;

	//Setup sched affinity to the core
	for (i = 0; i < REPETITIONS; i++) {
		struct timespec start_data, stop_data, empty_start, empty_stop;

		(syscall_method_arr[SYSCALLOPTION])(&start_data, &stop_data, &empty_start, &empty_stop);

		long long duration = timespec_subtract(&start_data,&stop_data)-timespec_subtract(&empty_start,&empty_stop);
		if(duration > 0){
		LAT[coreNumber][i] = duration;
		} else{
			printf("%d : %lld\n",i,duration);
		}
    }

}

void *run_stress_load(int core){
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core, &cpuset);
	pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&cpuset);

	int coreNumber = sched_getcpu();
	fprintf(stderr,"Core Number :%d\n",coreNumber);
	pthread_barrier_wait(&barrier);

	int i;

	//Setup sched affinity to the core
	for (i = 0; i < REPETITIONS; i++) {
		struct timespec start_data, stop_data, empty_start, empty_stop;

		run_openat(&start_data, &stop_data, &empty_start, &empty_stop);

    }
}

int main(int argc, char* argv[])
{
    int *addr;
    int i; 
	
	param.sched_priority = 99;
	
    int numStressThreads = 0;

    if(argc == 4){
    	REPETITIONS = atoi(argv[1]);
		numStressThreads = atoi(argv[2]);
		fprintf(stderr,"REPS overridden to %d\n",REPETITIONS);
		SYSCALLOPTION = atoi(argv[3]);
		RT_PRIORITY = atoi(argv[4]);
    }
	
	pthread_barrier_init(&barrier, NULL, numStressThreads+2);
	//start a thread which will run our getpid test

	//start a number of stress tests which will call open 
	//(we need to increase buffer size to handle the extra logs, probably needs to be controlled via the iteration count as well)

    i = numStressThreads;

	pthread_t threads[numStressThreads+1];

	for(i =1; i<= numStressThreads;i++)
	{
		pthread_create(&threads[i],NULL,run_stress_load	,3 - i);	//create the thread that will run the stress load
	}

	pthread_create(&threads[0],NULL,run_benchmark,3);	//the main thread will run the actual bechmark code

	pthread_barrier_wait(&barrier);


	for(i = 0; i <= numStressThreads;i++){
		pthread_join(threads[i],NULL);
	}

    int j;

	//We just need to do the reporting for thread running on core 3, the rest  are not important, we don't even need to collect the time for that tbh
    for(j=0; j < REPETITIONS; j++){
		printf("Net latency for core %d iteration %d is %lld ns\n",3,j,LAT[3][j]);
    }

    return 0;
}

