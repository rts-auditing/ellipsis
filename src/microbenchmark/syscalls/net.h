#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h> 
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

void run_connect(struct timespec* start_data,struct timespec* stop_data,struct timespec* empty_start,struct timespec* empty_stop){
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in result;
    result.sin_family = AF_INET; /* IPv4 only */
    result.sin_port = htons(80); /* TCP */

    if(inet_pton(AF_INET, "172.217.27.206", &result.sin_addr)<=0)  
    { 
        printf("\nInvalid address/ Address not supported \n"); 
        exit(1);
    }

    clock_gettime(CLOCK_MONOTONIC,start_data);
    if (connect(sock_fd, (struct sockaddr *)&result, sizeof(result)) < 0) {
        perror("connect");
        exit(2);
    }
    clock_gettime(CLOCK_MONOTONIC,stop_data);

    clock_gettime(CLOCK_MONOTONIC,empty_start);
    clock_gettime(CLOCK_MONOTONIC,empty_stop);
}
