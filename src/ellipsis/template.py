"""
Template.py contains functions that help benchmarks that need templates.
"""

def create_template_file(task_name,syscall_seq,intra_task_constraint=0,inter_task_constraint=0):
    tpl_filename = task_name + ".template"
    with open(tpl_filename,'w') as f:
        f.write(task_name + "\n")
        f.write(str(len(syscall_seq)) + "\n")
        f.write(str(intra_task_constraint) + "\n")
        f.write(str(inter_task_constraint) + "\n")
        for syscall in syscall_seq:
            f.write(str(syscall) + ":-1:-1:-1:-1:0\n")

    return tpl_filename
