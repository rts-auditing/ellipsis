import subprocess

CUSTOM_AUDITCTL_PATH = "/home/pi/audit-userspace/src/auditctl"

def toggle_ellipsis(enable,suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH,'-T',str(enable)],universal_newlines=True,capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def toggle_ellipsis2(enable,suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH,'-M',str(enable)],universal_newlines=True,capture_output=suppress_output)
    if not suppress_output:
        print("\n")


def load_template(template_file,suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH,'-z','file='+template_file],universal_newlines=True,capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def clear_templates(suppress_output=False):
    subprocess.run([CUSTOM_AUDITCTL_PATH,'-X'],universal_newlines=True,capture_output=suppress_output)
    if not suppress_output:
        print("\n")
    return

def get_status():
    subprocess.run([CUSTOM_AUDITCTL_PATH,'-s'],universal_newlines=True)
    print("\n")